import React from 'react';
import ReactDOM from 'react-dom';
import { ConfigProvider } from 'antd';

import App from './App';

import data from './data/data.json';

import 'antd/dist/antd.variable.min.css';
import './index.css';

ConfigProvider.config({
	theme: {
		primaryColor: data.dashboard.meta.primary_color,
	},
});

ReactDOM.render(
	<React.StrictMode>
		<ConfigProvider>
			<App
				title={data.dashboard.title}
				filters={data.dashboard.filters}
				initialApps={data.dashboard.apps}
			/>
		</ConfigProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
