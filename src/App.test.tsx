import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import data from './data/data.json';

it('Components will mount without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(
		<App
			title={data.dashboard.title}
			filters={data.dashboard.filters}
			initialApps={data.dashboard.apps}
		/>,
		div
	);
});
