import React, { FC, useEffect, useState } from 'react';
import { Card, Col, Layout, message, Modal, Row, Select, Space } from 'antd';
const { Content } = Layout;
const { Option } = Select;

import { DeleteOutlined } from '@ant-design/icons';
import { convertTimestampToDate } from './components/helpers';

import './App.css';

interface Filter {
	filter_id: string;
	field_name: string;
	title: string;
	disabled: boolean;
	appId: string;
	values: Array<string>;
}

interface App {
	app_id: number;
	app_name: Nullable<string>;
	app_description: string;
	app_type: string;
	last_update: string;
	image_preview: string;
}

type Props = {
	title: string;
	filters: Filter[];
	initialApps: App[];
};

type Nullable<T> = T | null;

const App: FC<Props> = ({ title, filters, initialApps }) => {
	const [apps, setApps] = useState(initialApps);
	const [activeFilter, setActiveFilter] = useState('');

	useEffect(() => {
		document.title = title;
	}, []);

	const handleFilterChange = (filterValue: string) => {
		setActiveFilter(filterValue);
	};

	const handleDeleteClick = (id: number) => {
		const appName = apps.find((app) => app.app_id === id);
		Modal.confirm({
			title: 'Are you sure to delete the app?',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			content: `App ${appName?.app_name} will be deleted.`,
			onOk: () => handleDeleteConfirm(id),
		});
	};

	const handleDeleteConfirm = async (id: number) => {
		return new Promise((resolve, reject) => {
			fetch(`https://622b294714ccb950d2312f3c.mockapi.io/apps/${id}`, {
				method: 'DELETE',
			})
				.then((res) => res.json())
				.then((data) => {
					const newApps = apps.filter((filter) => filter.app_id !== id);
					setApps(newApps);
					resolve(data);
				})
				.catch((err) => {
					message.error('Try again later...');
					reject(err);
				});
		});
	};

	return (
		<>
			<Layout>
				<Content style={{ maxWidth: '1200px', margin: '0 auto' }}>
					<Row style={{ textAlign: 'center', margin: '2em 0' }}>
						{filters.map((filter) => {
							return (
								<Col key={filter.filter_id} span={24}>
									<Space>
										{filter.title}
										<Select
											placeholder='select app type'
											onChange={handleFilterChange}
											disabled={filter.disabled}
										>
											{filter.values.map((value) => {
												return (
													<Option value={value} key={value}>
														{value}
													</Option>
												);
											})}
										</Select>
									</Space>
								</Col>
							);
						})}
					</Row>

					<Row gutter={10}>
						{apps
							.filter((app) => app.app_type === activeFilter || !activeFilter)
							.map((app) => {
								const title = app.app_name || 'No Title';
								return (
									<Col
										key={app.app_id}
										xs={{ span: 24 }}
										sm={{ span: 12 }}
										lg={{ span: 8 }}
										xl={{ span: 6 }}
										style={{ marginBottom: '10px', minWidth: '260px' }}
									>
										<Card
											hoverable
											style={{ width: '100%' }}
											actions={[
												<DeleteOutlined
													key={`delete${app.app_id}`}
													onClick={() => handleDeleteClick(app.app_id)}
												/>,
											]}
										>
											<h4>{title}</h4>
											<p>Last update: {convertTimestampToDate(app.last_update)}</p>
											<div className='imageWrapper'>
												<img src={app.image_preview} alt={title} />
											</div>
											<p className='description'>{app.app_description}</p>
											<p>Type: {app.app_type}</p>
										</Card>
									</Col>
								);
							})}
					</Row>
				</Content>
			</Layout>
		</>
	);
};

export default App;
