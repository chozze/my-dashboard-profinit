export const convertTimestampToDate = (timestamp: string) => {
	const milliseconds = parseInt(timestamp);
	const dateObject = new Date(milliseconds);
	const date = dateObject.toLocaleString('en-US');
	return date.slice(0, date.lastIndexOf(',')); //2019-12-9
};
